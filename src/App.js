import React from "react";
import "./components/fontawesome";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HashRouter } from 'react-router-dom';
import Login from './pages/login';

import Today from './pages/todayList';
import AllList from './pages/allList';

import PrivateRoute from './helpers/auth.js';
import './App.scss';

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
          <HashRouter>
            <div>
              <Route path="/" component={Login} exact />
              <PrivateRoute path="/today-list" component={Today} exact />
              <PrivateRoute path="/all-list" component={AllList} exact />
            </div>
          </HashRouter>
        </Switch>
      </BrowserRouter>
  );
};

export default App;