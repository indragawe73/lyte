import React, { useState, useEffect, useContext } from 'react'

import SideBar from './../../components/sideBar'
import Header from './../../components/header'
import Footer from './../../components/footer'

import Loading from './../../components/loading'
import Card from './../../components/card'

import { Store } from './../../store'
import { getTodayList } from './../../actions'
import './Today.scss';

const Today = () => {
  
  const { dispatch, state } = useContext(Store);
  const [loading, setLoading] = useState(false);
  const [myList, setMyList] = useState([]);

  useEffect (() => {
    setLoading(true)
    getTodayList('movie', dispatch)
  }, [dispatch])

  useEffect (() => {
    setMyList(state.todayList)
    setTimeout(() => {
      setLoading(false)
    }, 1500);
  }, [state.todayList])

  return (
    
    <>
      { loading && <Loading/> }
      <div className="wrapper">
        <SideBar />
        <div className="main-panel">    
            <Header />
            <div className="content">     

                <h2 className="text-center m-b-20"><b>Today - list !</b></h2>
               
                <div className="row m-t-10">
                  <div className="col-md-1"></div>
                  <div className="col-md-10">
                    <div className="row">
                      {myList && myList.length > 1 ?
                        <Card myList={myList} />
                        :null
                      }

                    </div>
                  </div>
                  <div className="col-md-1"></div>
                 
                </div>

            </div>

            <Footer />
        </div>
      </div>
    </>
  );
};

export default Today;
