import React, { useContext, useEffect, useState } from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import { useHistory } from "react-router-dom";

import { loginAuth } from './../../actions'
import { Store } from './../../store'
import Alert from './../../components/alert'
import Loading from './../../components/loading'

import './Login.scss';

const Login = () => {
  let history = useHistory();

  const { dispatch } = useContext(Store);
  const [alert, setAlert] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault()
    let userName = e.target[0].value;
    let password = e.target[1].value;

    if(userName && password) {
      loginAuth(userName, password, dispatch)
      const auth = { username: "Admin" };

      if( (userName === "qwe") && (password === "123") ) {
        sessionStorage.setItem('gcn-web', JSON.stringify(auth));
        history.push('/today-list')
      } else {
        setLoading(false)
        setAlert(true) 
        sessionStorage.removeItem('gcn-web');
      }

    } else {
      setAlert(true)
      console.log('onClick', alert)
    }
  }
  
  const handleAlert = () => {
     setAlert(false) 
  }

  useEffect (() => {
      const hostname = window && window.location && window.location.hostname;
      console.log('hostname', hostname)

  }, [])

  return (
    
    <React.Fragment>
      <div className="container-login">
        {loading && <Loading /> }
        {alert && <Alert onClick={handleAlert} title="Incorrect Username or Password!" text="Please input your correct Username or Password!"/> }
        <div className="wrap-login">
          <div className="login-btpns">
             <img 
              src="https://www.lyteventures.com/wp-content/themes/lvweb2/images/logo-white.png"
              alt="new"
              />
          </div>

          <form className="login-form" onSubmit={handleSubmit}>

            <div className="login-pic-logo">
            </div>
            <span className="login-form-title">
              Todo App Login
            </span>

            <div className="wrap-input">
              <input className="input" type="text" name="user" placeholder="user" />
              <span className="focus-input"></span>
              <span className="symbol-input">
                <FontAwesomeIcon icon="user" />
              </span>
            </div>

            <div className="wrap-input">
              <input className="input" type="password" name="pass" placeholder="Password" />
              <span className="focus-input"></span>
              <span className="symbol-input">
                <FontAwesomeIcon icon="lock" />
              </span>
            </div>
            
            <div className="container-login-form-btn">
              <button className="login-form-btn">
                Login
              </button>
            </div>

            <div className="text-center p-t-12">
              <span className="txt1">
                Forgot
              </span>
              <span className="txt2">
                Username / Password?
              </span>
            </div>

            <div className="text-center p-t-136">
              <span className="txt2">
                Create your Account
                <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
              </span>
            </div>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Login;
