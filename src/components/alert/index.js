import React from 'react'

import './Alert.scss';

const Alert = (props) => {
  
  let category = null
  const title = props.title
  const text = props.text
  const close = props.onClick

  if (props.category === 'success') {
		category = "alert-success"
  } else  if (props.category === 'warning') {
		category = "alert-warning"
  } else {
  	category = "alert-danger"
  }

  return (
		<div className={`alert invert ${category}`}>
		  <strong>{title}</strong> <br /> {text}
		  <button type="button" className="close" onClick={close}>
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
  );
};

export default Alert;

