import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useHistory } from "react-router-dom";

import './SideBar.scss';

const SideBar = (props) => {
  
  let history = useHistory();
  const [listNav, setListNav] = useState(1)

  useEffect (() => {
    const navActive = window.location.hash;
    if(navActive === '#/today-list') {
      setListNav(1)
    }
    else if(navActive === '#/all-list') {
      setListNav(2)
    }
    else {}

  }, [])
  
  const goToTodayList = () => {
    history.push('/today-list')
  }
  const goToAllList = () => {
    history.push('/all-list')
  }

  return (
  <>
    <div className="sidebar">
        <div className="sidebar-wrapper">
            <div className="logo">
                <span className="simple-text">
                    Lyte
                </span>
            </div>
            <ul className="nav">
                <li className="nav-item">
                    <span className={listNav === 1 ? "nav-link nav-link-active" :"nav-link"} onClick={goToTodayList}>
                        <FontAwesomeIcon  className="nc-icon" icon="clipboard" />
                        <p>Today List</p>
                    </span>
                </li>
                <li className="nav-item">
                    <span className={listNav === 2 ? "nav-link nav-link-active" :"nav-link"} onClick={goToAllList}>
                        <FontAwesomeIcon  className="nc-icon" icon="list" />
                        <p>All List</p>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div className="footbar">
        <div className="wrap-footbar">
          <div className={listNav === 1 ? "nav-active" : "xxx"} onClick={goToTodayList}>
            <FontAwesomeIcon  className="nc-icon" icon="clipboard" />
            <p className="text-footbar">Today List</p>
          </div>
        </div>
        <div className="wrap-footbar">
          <div className={listNav === 2 ? "nav-active" : "xxx"} onClick={goToAllList}>
            <FontAwesomeIcon  className="nc-icon" icon="list" />
            <p className="text-footbar">All List</p>
          </div>
        </div>
    </div>
  </>

  );
};

export default SideBar;
