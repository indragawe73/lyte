import React from 'react'

import './Loading.scss';

const Loading = (props) => {

  return (
		<div className="loading-area invert">
			<div className="loading-wrap">
			    <div className="loading">
			      <div></div>
			      <div></div>
			      <div></div>
			      <div></div>
			      <div></div>
			    </div>
			</div>
		</div>
  );
};

export default Loading;

