import React, { useContext, useState } from 'react'
import { useHistory } from "react-router-dom";

import { Store } from './../../store'
import { loginAuth } from './../../actions'

import './Header.scss';

const Header = (props) => {
  
  const [darkMode, setDarkMode] = useState(false);

  let history = useHistory();
  const { dispatch } = useContext(Store);
  
  const handleLogout = () => {
    loginAuth("xxx", "xxx", dispatch)
    history.push('/')
  };
  const handleDarkMode = () => {
      setDarkMode(!darkMode)
      document.documentElement.classList.toggle('dark-mode')
  };

  return (

    <nav className="navbar navbar-expand-lg " color-on-scroll="500">
        <div className="container-fluid nav-mobile">
            
          <li className="" onClick={handleDarkMode}>
              <span className="nav-link-2">
                  <span className="no-icon btn-dark-mode"> {darkMode ? `Dark Mode` : `Light Mode` }</span>
              </span>
          </li>
          <li className="">
              <span className="nav-link-2">
                  <span className="no-icon" onClick={handleLogout}>Log out</span>
              </span>
          </li>
        </div>
        
        <div className="container-fluid">
            <div className="collapse navbar-collapse justify-content-end">
                <ul className="nav navbar-nav mr-auto">
                    <li className="nav-item">
                        <span className="nav-link">
                            <span className="d-lg-none">Dashboard</span>
                        </span>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item" onClick={handleDarkMode}>
                        <span className="nav-link">
                            <span className="no-icon btn-dark-mode"> {darkMode ? `Dark Mode` : `Light Mode` }</span>
                        </span>
                    </li>
                    <li className="nav-item">
                        <span className="nav-link">
                            <span className="no-icon" onClick={handleLogout}>Log out</span>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

  );
};

export default Header;