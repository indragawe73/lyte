import { library } from '@fortawesome/fontawesome-svg-core'
import { faEnvelope, faUser, faLock, faSearch, faBell, faList, faClipboard, faUpload, faFolder, faPen, faTimes, faCheck } from '@fortawesome/free-solid-svg-icons'
 
library.add(faEnvelope, faUser, faLock, faSearch, faBell, faList, faClipboard, faUpload, faFolder, faPen, faTimes, faCheck)