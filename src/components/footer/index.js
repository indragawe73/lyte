import React from 'react'

import './Footer.scss';

const Footer = () => {

  return (

        <footer className="footer">
            <div className="container-fluid">
                <nav>
                    <p className="copyright text-center">
                        © <span>2021 Lyte Ventures</span>. All rights reserved.
                    </p>
                </nav>
            </div>
        </footer>

  );
};

export default Footer;