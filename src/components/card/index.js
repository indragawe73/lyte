import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import './Card.scss';

const Card = (props) => {

    const myList = props.myList

  return (
    <>


    {myList &&
        myList.map((item, idx) => {
            return (

              <div key={idx} className={"col-md-12 card-list " + (item.name === 'Action' || item.name === 'Comedy' ? 'card-list-prime' : item.name === 'Adventure' || item.name === 'Documentary' ? 'card-list-second' : 'card-list-third')}>
                <div className="list-icon">
                  <FontAwesomeIcon  className="nc-icon search-btn" icon="check" />
                </div>
                <div className="list-area">
                  <div className="list-title">{item.name}</div>
                  <div className="list-content">{item.id} - Sprint review and progres report to client on the spot {item.name}</div>
                  <div className="list-time">12:00 - 13:00</div>
                </div>
              </div>

            )

        })
    }


    </>
  );
};

export default Card;