import axiosConfig from './../../helpers/axios';
import { API_LINK, API_KEY } from './../../helpers/api'

export const getTodayList = (today, dispatch) => {
	let todayList;
    const linkUser = `${API_LINK}genre/${today}/list?api_key=${API_KEY}&language=en-US`
    axiosConfig.get(linkUser)
    .then(response => {
        todayList = response.data.genres
        dispatch({
            type: 'GET_TODAY_LIST',
            todayList,
        })
    })
    .catch(error => {
        console.log('axios error get("getBusinessFields")', error)
    })
}