export const getSessionStorage = (dispatch) => {
    
    let data = sessionStorage.getItem('gcn-web');

    if (data) {
      data = JSON.parse(data);
      dispatch({
        type: 'GET_SESSION_STORAGE',
        loginSession: data
      });
    }
};

export const loginAuth = (username, password, dispatch) => {
    
    const auth = { username: "Admin" };
    
    if( (username === "qwe") && (password === "123") ) {
      dispatch({
          type: 'LOGIN',
          auth,
      })
      sessionStorage.setItem('gcn-web', JSON.stringify(auth));
    } else {
      dispatch({ 
          type: 'LOGIN', 
          auth: "Login Error" 
      });
      sessionStorage.removeItem('gcn-web');
    }
};

export const logout = () => {
    sessionStorage.removeItem('gcn-web');
};
