const loginAuth = (state, auth) => {
    return { ...state, auth: auth };
};
const getSessionStorage = (state, loginSession) => {
    return { ...state, loginSession: loginSession };
};
const getTodayList = (state, todayList) => {
    return { ...state, todayList: todayList };
};

export const reducer = (state, action) => {
    switch (action.type) {
        case 'LOGIN':
            return loginAuth(state, action.auth);
        case 'GET_SESSION_STORAGE':
            return getSessionStorage(state, action.loginSession);
        case 'GET_TODAY_LIST':
            return getTodayList(state, action.todayList);
        default:
            return state;
    }
};