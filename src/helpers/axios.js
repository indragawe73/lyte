import axios from 'axios'
import { API_LINK } from './api'

const instance = axios.create({
    baseURL: API_LINK
    // baseURL: process.env.REACT_APP_API_LINK
});

// instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FROM INSTANCE';
instance.defaults.headers.post['Content-Type'] = 'application/json';

instance.interceptors.request.use(request => {
    return request;
}, error => {
    return Promise.reject(error);
});

instance.interceptors.response.use(response => {
    return response;
}, error => {
    return Promise.reject(error);
});


export default instance;